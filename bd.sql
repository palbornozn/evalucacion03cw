-- MariaDB dump 10.18  Distrib 10.4.17-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: fastdevelopment
-- ------------------------------------------------------
-- Server version	10.4.17-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `autores`
--

DROP TABLE IF EXISTS `autores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `autores` (
  `id_autor` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_autor`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `autores`
--

LOCK TABLES `autores` WRITE;
/*!40000 ALTER TABLE `autores` DISABLE KEYS */;
INSERT INTO `autores` VALUES (1,'J.R.R Tolkien',NULL),(2,'Gabriel Garcia Marquez',NULL),(3,'Albert Camus',NULL),(4,'Heather Morris',NULL);
/*!40000 ALTER TABLE `autores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `boleta`
--

DROP TABLE IF EXISTS `boleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `boleta` (
  `FolioBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `precioNEto` int(11) NOT NULL,
  `previoConIva` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `fechaDeVenta` datetime NOT NULL,
  `horaVenta` datetime NOT NULL,
  `id_trabajador` int(11) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`FolioBoleta`),
  KEY `id_trabajador` (`id_trabajador`),
  KEY `id_cliente` (`id_cliente`),
  CONSTRAINT `boleta_ibfk_1` FOREIGN KEY (`id_trabajador`) REFERENCES `trabajador` (`id_trabajador`),
  CONSTRAINT `boleta_ibfk_2` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `boleta`
--

LOCK TABLES `boleta` WRITE;
/*!40000 ALTER TABLE `boleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `boleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cabecerafactura`
--

DROP TABLE IF EXISTS `cabecerafactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cabecerafactura` (
  `id_cabeceraFactura` int(11) NOT NULL AUTO_INCREMENT,
  `rut_proveedor` int(11) NOT NULL,
  `numeroFactura` int(11) NOT NULL,
  `fechaFactura` datetime NOT NULL,
  `totalNeto` int(11) NOT NULL,
  `iva` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_cabeceraFactura`),
  KEY `rut_proveedor` (`rut_proveedor`),
  CONSTRAINT `cabecerafactura_ibfk_1` FOREIGN KEY (`rut_proveedor`) REFERENCES `distribuidores` (`rut_proveedor`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cabecerafactura`
--

LOCK TABLES `cabecerafactura` WRITE;
/*!40000 ALTER TABLE `cabecerafactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `cabecerafactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria`
--

DROP TABLE IF EXISTS `categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria` (
  `id_categoria` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_categoria`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria`
--

LOCK TABLES `categoria` WRITE;
/*!40000 ALTER TABLE `categoria` DISABLE KEYS */;
INSERT INTO `categoria` VALUES (1,'Fantasia',NULL),(2,'Novelas',NULL),(3,'Historia',NULL);
/*!40000 ALTER TABLE `categoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cliente`
--

DROP TABLE IF EXISTS `cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cliente` (
  `id_cliente` int(11) NOT NULL AUTO_INCREMENT,
  `DNI` varchar(250) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `fechaNacimiento` datetime NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_cliente`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cliente`
--

LOCK TABLES `cliente` WRITE;
/*!40000 ALTER TABLE `cliente` DISABLE KEYS */;
INSERT INTO `cliente` VALUES (1,'17635685-5','Patricio','Albornoz','1991-02-12 03:00:00',0),(4,'17635685-5','Juan ','Pedro','1998-02-14 03:00:00',0),(5,'7879013-k','Patricio','Jose','1995-05-13 04:00:00',0),(6,'21095327-2','Juan ','Peres','2021-07-09 04:00:00',0),(7,'7879013-k','Patricio','Albornoz','2021-07-01 04:00:00',0),(8,'7879013-k','Patricio','Albornoz','2021-07-01 04:00:00',0),(9,'7250190-k','Pedro','Jose','2020-11-04 03:00:00',0),(10,'18491122-1','Test','Cliente','2021-07-01 04:00:00',0);
/*!40000 ALTER TABLE `cliente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `comuna`
--

DROP TABLE IF EXISTS `comuna`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `comuna` (
  `id_comuna` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_comuna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `comuna`
--

LOCK TABLES `comuna` WRITE;
/*!40000 ALTER TABLE `comuna` DISABLE KEYS */;
/*!40000 ALTER TABLE `comuna` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contrato`
--

DROP TABLE IF EXISTS `contrato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contrato` (
  `id_contrato` int(11) NOT NULL AUTO_INCREMENT,
  `id_trabajador` int(11) NOT NULL,
  `fechaContrato` datetime NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_contrato`),
  KEY `id_trabajador` (`id_trabajador`),
  CONSTRAINT `contrato_ibfk_1` FOREIGN KEY (`id_trabajador`) REFERENCES `trabajador` (`id_trabajador`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contrato`
--

LOCK TABLES `contrato` WRITE;
/*!40000 ALTER TABLE `contrato` DISABLE KEYS */;
/*!40000 ALTER TABLE `contrato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `copialibros`
--

DROP TABLE IF EXISTS `copialibros`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `copialibros` (
  `id_copiaLibros` int(11) NOT NULL AUTO_INCREMENT,
  `cod_isbn` int(11) NOT NULL,
  `numeroSerie` int(11) NOT NULL,
  `destino` varchar(200) DEFAULT NULL,
  `id_precioLibro` int(11) NOT NULL,
  `id_estadoLibro` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_copiaLibros`),
  KEY `id_estadoLibro` (`id_estadoLibro`),
  KEY `cod_isbn` (`cod_isbn`),
  CONSTRAINT `copialibros_ibfk_1` FOREIGN KEY (`id_estadoLibro`) REFERENCES `estadolibro` (`id_estadoLibro`),
  CONSTRAINT `copialibros_ibfk_2` FOREIGN KEY (`cod_isbn`) REFERENCES `libro` (`cod_isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `copialibros`
--

LOCK TABLES `copialibros` WRITE;
/*!40000 ALTER TABLE `copialibros` DISABLE KEYS */;
INSERT INTO `copialibros` VALUES (1,77775548,1,'VENTA',1,1,NULL),(2,77775548,2,'VENTA',1,1,NULL),(3,77775548,3,'VENTA',1,1,NULL),(4,77775548,4,'ARRIENDO',2,1,NULL),(5,77775548,5,'ARRIENDO',2,1,NULL),(6,77775548,6,'ARRIENDO',2,1,NULL),(7,77788812,1,'VENTA',3,1,NULL),(8,77788812,2,'VENTA',3,1,NULL),(9,77788812,3,'VENTA',3,1,NULL),(10,77788812,4,'ARRIENDO',4,1,NULL),(11,77788812,5,'ARRIENDO',4,1,NULL),(12,77788812,6,'ARRIENDO',4,1,NULL),(13,77799955,1,'VENTA',5,1,NULL),(14,77799955,2,'VENTA',5,1,NULL),(15,77799955,3,'VENTA',5,1,NULL),(16,77799955,4,'ARRIENDO',6,1,NULL),(17,77799955,5,'ARRIENDO',6,1,NULL),(18,77799955,6,'ARRIENDO',6,1,NULL);
/*!40000 ALTER TABLE `copialibros` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `correos`
--

DROP TABLE IF EXISTS `correos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `correos` (
  `id_correos` int(11) NOT NULL AUTO_INCREMENT,
  `correo` varchar(300) NOT NULL,
  `id_trabajador` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_correos`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `correos`
--

LOCK TABLES `correos` WRITE;
/*!40000 ALTER TABLE `correos` DISABLE KEYS */;
/*!40000 ALTER TABLE `correos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detalleboleta`
--

DROP TABLE IF EXISTS `detalleboleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detalleboleta` (
  `id_detalleBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `FolioBoleta` int(11) NOT NULL,
  `id_copiaLibros` int(11) NOT NULL,
  `valorLibro` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_detalleBoleta`),
  KEY `FolioBoleta` (`FolioBoleta`),
  KEY `id_copiaLibros` (`id_copiaLibros`),
  CONSTRAINT `detalleboleta_ibfk_1` FOREIGN KEY (`FolioBoleta`) REFERENCES `boleta` (`FolioBoleta`),
  CONSTRAINT `detalleboleta_ibfk_2` FOREIGN KEY (`id_copiaLibros`) REFERENCES `copialibros` (`id_copiaLibros`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detalleboleta`
--

LOCK TABLES `detalleboleta` WRITE;
/*!40000 ALTER TABLE `detalleboleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `detalleboleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `detallefactura`
--

DROP TABLE IF EXISTS `detallefactura`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `detallefactura` (
  `id_detalleFactura` int(11) NOT NULL AUTO_INCREMENT,
  `id_cabeceraFactura` int(11) NOT NULL,
  `cod_isbn` int(11) NOT NULL,
  `precioCompra` int(11) NOT NULL,
  `cantidad` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_detalleFactura`),
  KEY `id_cabeceraFactura` (`id_cabeceraFactura`),
  CONSTRAINT `detallefactura_ibfk_1` FOREIGN KEY (`id_cabeceraFactura`) REFERENCES `cabecerafactura` (`id_cabeceraFactura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `detallefactura`
--

LOCK TABLES `detallefactura` WRITE;
/*!40000 ALTER TABLE `detallefactura` DISABLE KEYS */;
/*!40000 ALTER TABLE `detallefactura` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `devolucion`
--

DROP TABLE IF EXISTS `devolucion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `devolucion` (
  `id_devolucion` int(11) NOT NULL AUTO_INCREMENT,
  `id_prestamo` int(11) NOT NULL,
  `devolucionReal` datetime DEFAULT NULL,
  `diasMulta` int(11) DEFAULT NULL,
  `precioMulta` int(11) DEFAULT NULL,
  `precioFinal` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_devolucion`),
  KEY `id_prestamo` (`id_prestamo`),
  CONSTRAINT `devolucion_ibfk_1` FOREIGN KEY (`id_prestamo`) REFERENCES `prestamo` (`id_prestamo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `devolucion`
--

LOCK TABLES `devolucion` WRITE;
/*!40000 ALTER TABLE `devolucion` DISABLE KEYS */;
/*!40000 ALTER TABLE `devolucion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `direcciones`
--

DROP TABLE IF EXISTS `direcciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `direcciones` (
  `id_direcciones` int(11) NOT NULL AUTO_INCREMENT,
  `direccion` varchar(300) NOT NULL,
  `id_trabajador` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  `id_comuna` int(11) NOT NULL,
  PRIMARY KEY (`id_direcciones`),
  KEY `id_comuna` (`id_comuna`),
  CONSTRAINT `direcciones_ibfk_1` FOREIGN KEY (`id_comuna`) REFERENCES `comuna` (`id_comuna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `direcciones`
--

LOCK TABLES `direcciones` WRITE;
/*!40000 ALTER TABLE `direcciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `direcciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `distribuidores`
--

DROP TABLE IF EXISTS `distribuidores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `distribuidores` (
  `rut_proveedor` int(11) NOT NULL AUTO_INCREMENT,
  `razonSocial` varchar(300) NOT NULL,
  `direccion` varchar(300) NOT NULL,
  `id_comuna` int(11) NOT NULL,
  `fono` varchar(300) DEFAULT NULL,
  `añoVenta` datetime NOT NULL,
  `correo` varchar(300) DEFAULT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`rut_proveedor`),
  KEY `id_comuna` (`id_comuna`),
  CONSTRAINT `distribuidores_ibfk_1` FOREIGN KEY (`id_comuna`) REFERENCES `comuna` (`id_comuna`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `distribuidores`
--

LOCK TABLES `distribuidores` WRITE;
/*!40000 ALTER TABLE `distribuidores` DISABLE KEYS */;
/*!40000 ALTER TABLE `distribuidores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `editorial`
--

DROP TABLE IF EXISTS `editorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `editorial` (
  `id_editorial` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_editorial`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `editorial`
--

LOCK TABLES `editorial` WRITE;
/*!40000 ALTER TABLE `editorial` DISABLE KEYS */;
INSERT INTO `editorial` VALUES (1,'DEBOLSILLO',NULL),(2,'MINOTAURO',NULL),(3,'PLANETALECTOR',NULL),(4,'EMECE',NULL);
/*!40000 ALTER TABLE `editorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `estadolibro`
--

DROP TABLE IF EXISTS `estadolibro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `estadolibro` (
  `id_estadoLibro` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_estadoLibro`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `estadolibro`
--

LOCK TABLES `estadolibro` WRITE;
/*!40000 ALTER TABLE `estadolibro` DISABLE KEYS */;
INSERT INTO `estadolibro` VALUES (1,'DISPONIBLE',NULL),(2,'ARRENDADO',NULL),(3,'VENDIDO',NULL),(4,'DE BAJA',NULL);
/*!40000 ALTER TABLE `estadolibro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `idioma`
--

DROP TABLE IF EXISTS `idioma`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `idioma` (
  `id_idioma` int(11) NOT NULL AUTO_INCREMENT,
  `nombre` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_idioma`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `idioma`
--

LOCK TABLES `idioma` WRITE;
/*!40000 ALTER TABLE `idioma` DISABLE KEYS */;
INSERT INTO `idioma` VALUES (1,'Español',NULL);
/*!40000 ALTER TABLE `idioma` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libro`
--

DROP TABLE IF EXISTS `libro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `libro` (
  `cod_isbn` int(11) NOT NULL,
  `id_idioma` int(11) NOT NULL,
  `año_publicacion` date DEFAULT NULL,
  `id_editorial` int(11) NOT NULL,
  `titulo` varchar(300) NOT NULL,
  `contador_serie` int(11) NOT NULL,
  `paginas` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`cod_isbn`),
  KEY `id_idioma` (`id_idioma`),
  KEY `id_editorial` (`id_editorial`),
  CONSTRAINT `libro_ibfk_1` FOREIGN KEY (`id_idioma`) REFERENCES `idioma` (`id_idioma`),
  CONSTRAINT `libro_ibfk_2` FOREIGN KEY (`id_editorial`) REFERENCES `editorial` (`id_editorial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libro`
--

LOCK TABLES `libro` WRITE;
/*!40000 ALTER TABLE `libro` DISABLE KEYS */;
INSERT INTO `libro` VALUES (77775548,1,'2018-09-01',4,'EL TATUADOR DE AUSCHWITZ',10,309,NULL),(77788812,1,'1942-01-01',3,'EL EXTRANJERO',4,107,NULL),(77799955,1,'1966-01-01',2,'EL SEÑOR DE LOS ANILLOS Pt1',6,567,NULL);
/*!40000 ALTER TABLE `libro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `libroautor`
--

DROP TABLE IF EXISTS `libroautor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `libroautor` (
  `id_libroAutor` int(11) NOT NULL AUTO_INCREMENT,
  `id_autor` int(11) NOT NULL,
  `cod_isbn` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_libroAutor`),
  KEY `id_autor` (`id_autor`),
  KEY `cod_isbn` (`cod_isbn`),
  CONSTRAINT `libroautor_ibfk_1` FOREIGN KEY (`id_autor`) REFERENCES `autores` (`id_autor`),
  CONSTRAINT `libroautor_ibfk_2` FOREIGN KEY (`cod_isbn`) REFERENCES `libro` (`cod_isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `libroautor`
--

LOCK TABLES `libroautor` WRITE;
/*!40000 ALTER TABLE `libroautor` DISABLE KEYS */;
INSERT INTO `libroautor` VALUES (1,4,77775548,NULL),(2,3,77788812,NULL),(3,1,77799955,NULL);
/*!40000 ALTER TABLE `libroautor` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `librocategoria`
--

DROP TABLE IF EXISTS `librocategoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `librocategoria` (
  `id_libroCategoria` int(11) NOT NULL AUTO_INCREMENT,
  `id_categoria` int(11) NOT NULL,
  `cod_isbn` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_libroCategoria`),
  KEY `id_categoria` (`id_categoria`),
  KEY `cod_isbn` (`cod_isbn`),
  CONSTRAINT `librocategoria_ibfk_1` FOREIGN KEY (`id_categoria`) REFERENCES `categoria` (`id_categoria`),
  CONSTRAINT `librocategoria_ibfk_2` FOREIGN KEY (`cod_isbn`) REFERENCES `libro` (`cod_isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `librocategoria`
--

LOCK TABLES `librocategoria` WRITE;
/*!40000 ALTER TABLE `librocategoria` DISABLE KEYS */;
INSERT INTO `librocategoria` VALUES (1,2,77775548,NULL),(2,2,77788812,NULL),(3,1,77799955,NULL);
/*!40000 ALTER TABLE `librocategoria` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metodopago`
--

DROP TABLE IF EXISTS `metodopago`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metodopago` (
  `id_metedoPago` int(11) NOT NULL AUTO_INCREMENT,
  `metodo` varchar(300) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_metedoPago`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metodopago`
--

LOCK TABLES `metodopago` WRITE;
/*!40000 ALTER TABLE `metodopago` DISABLE KEYS */;
INSERT INTO `metodopago` VALUES (1,'Efectivo',NULL),(2,'Debito',NULL),(3,'Credito',NULL);
/*!40000 ALTER TABLE `metodopago` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `metodopagoboleta`
--

DROP TABLE IF EXISTS `metodopagoboleta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `metodopagoboleta` (
  `id_metodoPagoBoleta` int(11) NOT NULL AUTO_INCREMENT,
  `FolioBoleta` int(11) NOT NULL,
  `id_metedoPago` int(11) NOT NULL,
  `metodoMonto` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_metodoPagoBoleta`),
  KEY `FolioBoleta` (`FolioBoleta`),
  KEY `id_metedoPago` (`id_metedoPago`),
  CONSTRAINT `metodopagoboleta_ibfk_1` FOREIGN KEY (`FolioBoleta`) REFERENCES `boleta` (`FolioBoleta`),
  CONSTRAINT `metodopagoboleta_ibfk_2` FOREIGN KEY (`id_metedoPago`) REFERENCES `metodopago` (`id_metedoPago`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `metodopagoboleta`
--

LOCK TABLES `metodopagoboleta` WRITE;
/*!40000 ALTER TABLE `metodopagoboleta` DISABLE KEYS */;
/*!40000 ALTER TABLE `metodopagoboleta` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `preciolibro`
--

DROP TABLE IF EXISTS `preciolibro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `preciolibro` (
  `id_precioLibro` int(11) NOT NULL AUTO_INCREMENT,
  `precio` int(11) NOT NULL,
  `id_tipoPrecio` int(11) NOT NULL,
  `cod_isbn` int(11) NOT NULL,
  `fechaModificacion` date DEFAULT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_precioLibro`),
  KEY `id_tipoPrecio` (`id_tipoPrecio`),
  KEY `cod_isbn` (`cod_isbn`),
  CONSTRAINT `preciolibro_ibfk_1` FOREIGN KEY (`id_tipoPrecio`) REFERENCES `tipoprecio` (`id_tipoPrecio`),
  CONSTRAINT `preciolibro_ibfk_2` FOREIGN KEY (`cod_isbn`) REFERENCES `libro` (`cod_isbn`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `preciolibro`
--

LOCK TABLES `preciolibro` WRITE;
/*!40000 ALTER TABLE `preciolibro` DISABLE KEYS */;
INSERT INTO `preciolibro` VALUES (1,10000,1,77775548,NULL,NULL),(2,5000,2,77775548,NULL,NULL),(3,5000,1,77788812,NULL,NULL),(4,4000,2,77788812,NULL,NULL),(5,15000,1,77799955,NULL,NULL),(6,5000,2,77799955,NULL,NULL);
/*!40000 ALTER TABLE `preciolibro` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `prestamo`
--

DROP TABLE IF EXISTS `prestamo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `prestamo` (
  `id_prestamo` int(11) NOT NULL AUTO_INCREMENT,
  `id_cliente` int(11) NOT NULL,
  `id_trabajador` int(11) NOT NULL,
  `fechaPrestamo` datetime NOT NULL,
  `fechaDevolucion` datetime NOT NULL,
  `id_copiaLibros` int(11) NOT NULL,
  `diasArriendo` int(11) NOT NULL,
  `id_precioLibro` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_prestamo`),
  KEY `id_cliente` (`id_cliente`),
  KEY `id_trabajador` (`id_trabajador`),
  KEY `id_copiaLibros` (`id_copiaLibros`),
  KEY `id_precioLibro` (`id_precioLibro`),
  CONSTRAINT `prestamo_ibfk_1` FOREIGN KEY (`id_cliente`) REFERENCES `cliente` (`id_cliente`),
  CONSTRAINT `prestamo_ibfk_2` FOREIGN KEY (`id_trabajador`) REFERENCES `trabajador` (`id_trabajador`),
  CONSTRAINT `prestamo_ibfk_3` FOREIGN KEY (`id_copiaLibros`) REFERENCES `copialibros` (`id_copiaLibros`),
  CONSTRAINT `prestamo_ibfk_4` FOREIGN KEY (`id_precioLibro`) REFERENCES `preciolibro` (`id_precioLibro`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `prestamo`
--

LOCK TABLES `prestamo` WRITE;
/*!40000 ALTER TABLE `prestamo` DISABLE KEYS */;
/*!40000 ALTER TABLE `prestamo` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `telefonos`
--

DROP TABLE IF EXISTS `telefonos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `telefonos` (
  `id_telefono` int(11) NOT NULL AUTO_INCREMENT,
  `telefono` varchar(300) NOT NULL,
  `id_trabajador` int(11) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_telefono`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `telefonos`
--

LOCK TABLES `telefonos` WRITE;
/*!40000 ALTER TABLE `telefonos` DISABLE KEYS */;
/*!40000 ALTER TABLE `telefonos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipoprecio`
--

DROP TABLE IF EXISTS `tipoprecio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipoprecio` (
  `id_tipoPrecio` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(200) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_tipoPrecio`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipoprecio`
--

LOCK TABLES `tipoprecio` WRITE;
/*!40000 ALTER TABLE `tipoprecio` DISABLE KEYS */;
INSERT INTO `tipoprecio` VALUES (1,'Venta',NULL),(2,'Arriendo',NULL),(3,'Compra',NULL);
/*!40000 ALTER TABLE `tipoprecio` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipousuario`
--

DROP TABLE IF EXISTS `tipousuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipousuario` (
  `id_tipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `descripcion` varchar(300) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_tipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipousuario`
--

LOCK TABLES `tipousuario` WRITE;
/*!40000 ALTER TABLE `tipousuario` DISABLE KEYS */;
INSERT INTO `tipousuario` VALUES (1,'ADMINISTRADOR',NULL),(2,'USUARIO',NULL);
/*!40000 ALTER TABLE `tipousuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `trabajador`
--

DROP TABLE IF EXISTS `trabajador`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `trabajador` (
  `id_trabajador` int(11) NOT NULL AUTO_INCREMENT,
  `DNI` varchar(250) NOT NULL,
  `nombre` varchar(300) NOT NULL,
  `apellidos` varchar(300) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id_trabajador`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `trabajador`
--

LOCK TABLES `trabajador` WRITE;
/*!40000 ALTER TABLE `trabajador` DISABLE KEYS */;
INSERT INTO `trabajador` VALUES (1,'000000-0','ADMIN','SISTEMA',NULL),(2,'17635685-5','PATRICIO','ALBORNOZ',NULL),(3,'11572257-3','Pablo','Morales',0),(4,'15889399-1','Juan Pablo','Tormenata',0),(26,'17093262-5','Test','Trabajador',0);
/*!40000 ALTER TABLE `trabajador` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `Contraseña` varchar(300) NOT NULL,
  `id_tipoUsuario` int(11) NOT NULL,
  `id_trabajador` int(11) NOT NULL,
  `estado_registro` tinyint(1) DEFAULT NULL,
  `nombreUsuario` varchar(150) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`),
  KEY `id_tipoUsuario` (`id_tipoUsuario`),
  KEY `id_trabajador` (`id_trabajador`),
  CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`id_tipoUsuario`) REFERENCES `tipousuario` (`id_tipoUsuario`),
  CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`id_trabajador`) REFERENCES `trabajador` (`id_trabajador`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES (1,'fast12345',1,1,NULL,'admin'),(2,'12345',2,2,NULL,'palbornoz');
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-07-22 22:52:21
