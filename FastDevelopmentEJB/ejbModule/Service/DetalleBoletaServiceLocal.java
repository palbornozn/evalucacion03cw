package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.DetalleBoleta;

@Local
public interface DetalleBoletaServiceLocal {
	
	
	public void insertDetalleBoleta(DetalleBoleta d);
	public List<DetalleBoleta> listarDettaleBoletas();
	public void updateDetalleBoleta(DetalleBoleta d);

}
