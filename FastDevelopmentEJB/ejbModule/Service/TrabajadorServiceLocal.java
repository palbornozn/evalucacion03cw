package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.Trabajador;

@Local
public interface TrabajadorServiceLocal {
	
	public void insertTrabajador(Trabajador t);
	public List<Trabajador>listaTrabajador();
	public void deleteTrabajador(Trabajador t);
	public void updateTrabajador(Trabajador t);

}
