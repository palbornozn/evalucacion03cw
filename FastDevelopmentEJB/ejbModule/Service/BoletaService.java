package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Boleta;

/**
 * Session Bean implementation class BoletaService
 */
@Stateless
@LocalBean
public class BoletaService implements BoletaServiceLocal {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public BoletaService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertBoleta(Boleta b) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(b);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
			
	}
	

	@Override
	public List<Boleta> listaBoleta() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("Boleta.getAll",Boleta.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
		
	}

}
