package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Cliente;
import cl.inacap.data.CopiaLibros;

/**
 * Session Bean implementation class CopiaLibrosService
 */
@Stateless
@LocalBean
public class CopiaLibrosService implements CopiaLibrosServiceLocal {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public CopiaLibrosService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertCopialibros(CopiaLibros cl) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(cl);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
			
	}

	@Override
	public List<CopiaLibros> listaCopiaLibros() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("CopiaLibros.getAll",CopiaLibros.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
		
	}

	@Override
	public void deleteCopiaLibros(CopiaLibros cl) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void updateCopuaLibros(CopiaLibros cl) {
		// TODO Auto-generated method stub
		
		EntityManager em = this.emf.createEntityManager();
		try {
			
			CopiaLibros original = em.find(CopiaLibros.class, cl.getId_copiaLibros());
	
			original.setDestino(cl.getDestino());
			original.setEstado_registro(cl.isEstado_registro());
			original.setId_copiaLibros(cl.getId_copiaLibros());
			original.setId_estadoLibro(cl.getId_estadoLibro());
			original.setNumeroSerie(cl.getNumeroSerie());
			original.setPrecioLibro(cl.getPrecioLibro());
			
			em.merge(original);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
		
		
		
	}

}
