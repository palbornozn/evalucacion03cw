package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Cliente;
import cl.inacap.data.DetalleBoleta;

/**
 * Session Bean implementation class DetalleBoletaService
 */
@Stateless
@LocalBean
public class DetalleBoletaService implements DetalleBoletaServiceLocal {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public DetalleBoletaService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertDetalleBoleta(DetalleBoleta d) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(d);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
			
	}

	@Override
	public List<DetalleBoleta> listarDettaleBoletas() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("DetalleBoleta.getAll",DetalleBoleta.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
		
	}

	@Override
	public void updateDetalleBoleta(DetalleBoleta d) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			DetalleBoleta original = em.find(DetalleBoleta.class, d.getId_detalleBoleta());
			
			original.setBoleta(d.getBoleta());
			original.setCopialibro(d.getCopialibro());
			original.setEstado_registro(d.isEstado_registro());
			original.setId_detalleBoleta(d.getId_detalleBoleta());
			original.setValorLibro(d.getValorLibro());
			
			
			em.merge(original);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
	}

}
