package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import cl.inacap.data.Cliente;

/**
 * Session Bean implementation class ClienteService
 */
@Stateless
@LocalBean
public class ClienteService implements ClienteServiceLocal {

	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public ClienteService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public void insertCliente(Cliente c) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(c);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
			
	}

	@Override
	public List<Cliente> listarCliente() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("Cliente.getAll",Cliente.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
		
	}

	@Override
	public void deleteCliente(Cliente c) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			em.remove(em.find(Cliente.class, c.getId_cliente()));
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
	}

	@Override
	public void updateCliente(Cliente c) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			Cliente original = em.find(Cliente.class, c.getId_cliente());
	
			original.setApellidos(c.getApellidos());;
			original.setDNI(c.getDNI());
			original.setEstado_registro(c.isEstado_registro());
			original.setFechaNacimiento(c.getFechaNacimiento());
			original.setId_cliente(c.getId_cliente());
			original.setNombre(c.getNombre());
			
			em.merge(original);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
		
	}

}
