package Service;

import java.util.List;

import javax.ejb.Local;

import cl.inacap.data.Boleta;

@Local
public interface BoletaServiceLocal {
	
	public void insertBoleta(Boleta b);
	public List<Boleta> listaBoleta();
	
	

}
