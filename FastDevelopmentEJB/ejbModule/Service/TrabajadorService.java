package Service;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import cl.inacap.data.Trabajador;

/**
 * Session Bean implementation class TrabajadorService
 */
@Stateless
@LocalBean
public class TrabajadorService implements TrabajadorServiceLocal {
	private EntityManagerFactory emf = Persistence.createEntityManagerFactory("FastDevelopmentEJB");
    /**
     * Default constructor. 
     */
    public TrabajadorService() {
        // TODO Auto-generated constructor stub
    	
    }

	@Override
	public void insertTrabajador(Trabajador t) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			
			em.persist(t);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
		
	}

	@Override
	public List<Trabajador> listaTrabajador() {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			return em.createNamedQuery("Trabajador.getAll",Trabajador.class).getResultList();
			
			}catch(Exception ex) {
				return null;
				}finally {
					em.close();
				}
	}

	@Override
	public void deleteTrabajador(Trabajador t) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			em.remove(em.find(Trabajador.class, t.getId_trabajador()));
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
	}

	@Override
	public void updateTrabajador(Trabajador t) {
		// TODO Auto-generated method stub
		EntityManager em = this.emf.createEntityManager();
		try {
			
			Trabajador original = em.find(Trabajador.class, t.getId_trabajador());
	
			original.setApellidos(t.getApellidos());
			original.setDNI(t.getDNI());;
			original.setEstado_registro(t.isEstado_registro());
			original.setId_trabajador(t.getId_trabajador());
			original.setNombre(t.getNombre());
			
			em.merge(original);
			em.flush();
			
			}catch(Exception ex) {
				
				}finally {
					em.close();
				}
	}

}
