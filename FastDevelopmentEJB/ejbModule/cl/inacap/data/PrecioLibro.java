package cl.inacap.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name="precioLibro")

@NamedQueries({
	@NamedQuery(name="PrecioLibro.getAll",query="select p from PrecioLibro p")
	
})

public class PrecioLibro implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_precioLibro;
	
	@Column(name="precio")
	private int precio;
	
	@Column(name="id_tipoPrecio")
	private int id_tipoPrecio;
	
	@Column(name="cod_isbn")
	private int cod_isbn;
	
	@Column(name="fechaModificacion")
	private Date fechaModificacion;
	
	@Column(name="estado_registro")
	private boolean estado_registro;
	
	
	public int getId_precioLibro() {
		return id_precioLibro;
	}
	public void setId_precioLibro(int id_precioLibro) {
		this.id_precioLibro = id_precioLibro;
	}
	public int getPrecio() {
		return precio;
	}
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	public int getId_tipoPrecio() {
		return id_tipoPrecio;
	}
	public void setId_tipoPrecio(int id_tipoPrecio) {
		this.id_tipoPrecio = id_tipoPrecio;
	}
	public int getCod_isbn() {
		return cod_isbn;
	}
	public void setCod_isbn(int cod_isbn) {
		this.cod_isbn = cod_isbn;
	}
	public Date getFechaModificacion() {
		return fechaModificacion;
	}
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}
	
	
	
	
	
}
