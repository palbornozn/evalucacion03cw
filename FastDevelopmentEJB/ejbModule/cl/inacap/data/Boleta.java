package cl.inacap.data;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table (name="boleta")

@NamedQueries({
	@NamedQuery(name="Boleta.getAll",query="select b from Boleta b")
	
})
public class Boleta implements Serializable {

	 
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int FolioBoleta;
	
	@Column(name="precioNeto")
	private int precioNeto;
	@Column(name="precioConIva")
	private int precioConIva;
	@Column(name="total")
	private int total;
	@Column(name="fechaDeVenta")
	private LocalDateTime fechaDeVenta;
	@Column(name="horaVenta")
	private LocalDateTime horaVenta;
	@ManyToOne
	@JoinColumn(name="id_trabajador")
	Trabajador trabajador;
	@ManyToOne
	@JoinColumn(name="id_cliente")
	Cliente cliente;
	
	
	public int getFolioBoleta() {
		return FolioBoleta;
	}
	public void setFolioBoleta(int folioBoleta) {
		FolioBoleta = folioBoleta;
	}
	public int getPrecioNeto() {
		return precioNeto;
	}
	public void setPrecioNeto(int precioNeto) {
		this.precioNeto = precioNeto;
	}
	public int getPrecioConIva() {
		return precioConIva;
	}
	public void setPrecioConIva(int precioConIva) {
		this.precioConIva = precioConIva;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}




	public Trabajador getTrabajador() {
		return trabajador;
	}
	public void setTrabajador(Trabajador trabajador) {
		this.trabajador = trabajador;
	}
	public Cliente getCliente() {
		return cliente;
	}
	public void setCliente(Cliente cliente) {
		this.cliente = cliente;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}


	public LocalDateTime getFechaDeVenta() {
		return fechaDeVenta;
	}
	public void setFechaDeVenta(LocalDateTime fechaDeVenta) {
		this.fechaDeVenta = fechaDeVenta;
	}
	public LocalDateTime getHoraVenta() {
		return horaVenta;
	}
	public void setHoraVenta(LocalDateTime horaVenta) {
		this.horaVenta = horaVenta;
	}


	private boolean estado_registro;
	
	
}
