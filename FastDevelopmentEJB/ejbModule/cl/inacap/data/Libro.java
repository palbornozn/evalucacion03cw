package cl.inacap.data;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="libro")

@NamedQueries({
	@NamedQuery(name="Libro.getAll",query="select l from Libro l")
	
})

public class Libro implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int cod_isbn;
	
	@Column(name="id_idioma")
	private int id_idioma;
	
	@Column(name="a�o_publicacion")
	private Date a�o_publicacion;
	
	@Column(name="id_editorial")
	private int id_editorial;
	
	@Column(name="titulo")
	private String titulo;
	
	@Column(name="contador_serie")
	private int contador_serie;
	
	@Column(name="paginas")
	private int paginas;
	
	@Column(name="estado_registro")
	private boolean estado_registro;
	
	
	public int getCod_isbn() {
		return cod_isbn;
	}
	public void setCod_isbn(int cod_isbn) {
		this.cod_isbn = cod_isbn;
	}
	public int getId_idioma() {
		return id_idioma;
	}
	public void setId_idioma(int id_idioma) {
		this.id_idioma = id_idioma;
	}
	public Date getA�o_publicacion() {
		return a�o_publicacion;
	}
	public void setA�o_publicacion(Date a�o_publicacion) {
		this.a�o_publicacion = a�o_publicacion;
	}
	public int getId_editorial() {
		return id_editorial;
	}
	public void setId_editorial(int id_editorial) {
		this.id_editorial = id_editorial;
	}
	public String getTitulo() {
		return titulo;
	}
	public void setTitulo(String titulo) {
		this.titulo = titulo;
	}
	public int getContador_serie() {
		return contador_serie;
	}
	public void setContador_serie(int contador_serie) {
		this.contador_serie = contador_serie;
	}
	public int getPaginas() {
		return paginas;
	}
	public void setPaginas(int paginas) {
		this.paginas = paginas;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}
	
	
	
}
