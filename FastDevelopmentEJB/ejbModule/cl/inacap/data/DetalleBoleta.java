package cl.inacap.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table (name="detalleboleta")

@NamedQueries({
	@NamedQuery(name="DetalleBoleta.getAll",query="select d from DetalleBoleta d")
	
})
public class DetalleBoleta implements Serializable{
		
		@Id
		@GeneratedValue(strategy = GenerationType.IDENTITY)
		private int id_detalleBoleta;
		
		@ManyToOne
		@JoinColumn(name="FolioBoleta")
		Boleta boleta;
		
		@ManyToOne
		@JoinColumn(name="id_copiaLibros")
		CopiaLibros copialibro;
		
		@Column(name="valorLibro")
		private int valorLibro;
		@Column(name="estado_registro")
		private boolean estado_registro;
		
		public int getId_detalleBoleta() {
			return id_detalleBoleta;
		}
		public void setId_detalleBoleta(int id_detalleBoleta) {
			this.id_detalleBoleta = id_detalleBoleta;
		}

		public Boleta getBoleta() {
			return boleta;
		}
		public void setBoleta(Boleta boleta) {
			this.boleta = boleta;
		}
		public CopiaLibros getCopialibro() {
			return copialibro;
		}
		public void setCopialibro(CopiaLibros copialibro) {
			this.copialibro = copialibro;
		}
		public int getValorLibro() {
			return valorLibro;
		}
		public void setValorLibro(int valorLibro) {
			this.valorLibro = valorLibro;
		}
		public boolean isEstado_registro() {
			return estado_registro;
		}
		public void setEstado_registro(boolean estado_registro) {
			this.estado_registro = estado_registro;
		}
	
}
