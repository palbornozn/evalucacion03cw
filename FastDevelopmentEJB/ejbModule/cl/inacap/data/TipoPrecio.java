package cl.inacap.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table (name="tipoprecio")

@NamedQueries({
	@NamedQuery(name="TipoPrecio.getAll",query="select tp from TipoPrecio tp")
	
})

public class TipoPrecio implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_tipoPrecio;
	
	@Column(name="descripcion")
	private String descripcion;
	
	@Column(name="estado_registro")
	private boolean estado_registro;
	
	
	
	public int getId_tipoPrecio() {
		return id_tipoPrecio;
	}
	public void setId_tipoPrecio(int id_tipoPrecio) {
		this.id_tipoPrecio = id_tipoPrecio;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}
	
	

}
