package cl.inacap.data;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;


@Entity
@Table (name="usuarios")

@NamedQueries({
	@NamedQuery(name="Usuarios.getAll",query="select u from Usuarios u")
	
})
public class Usuarios implements Serializable {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id_usuario;
	
	@Column(name="contraseña")
	private String contraseña;
	
	@Column(name="id_tipoUsuario")
	private int id_tipoUsuario;
	
	@Column(name="id_trabajador")
	private int id_trabajador;
	
	@Column(name="estado_registro")
	private boolean estado_registro;
	
	@Column(name="nombreUsuario")
	private String nombreUsuario;
	
	
	
	
	public int getId_usuario() {
		return id_usuario;
	}
	public void setId_usuario(int id_usuario) {
		this.id_usuario = id_usuario;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public int getId_tipoUsuario() {
		return id_tipoUsuario;
	}
	public void setId_tipoUsuario(int id_tipoUsuario) {
		this.id_tipoUsuario = id_tipoUsuario;
	}
	public int getId_trabajador() {
		return id_trabajador;
	}
	public void setId_trabajador(int id_trabajador) {
		this.id_trabajador = id_trabajador;
	}
	public boolean isEstado_registro() {
		return estado_registro;
	}
	public void setEstado_registro(boolean estado_registro) {
		this.estado_registro = estado_registro;
	}
	public String getNombreUsuario() {
		return nombreUsuario;
	}
	public void setNombreUsuario(String nombreUsuario) {
		this.nombreUsuario = nombreUsuario;
	}
	
	
	
	
	
	
	
}
