<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>FastDevelopment</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Roboto+Slab:400,700|Material+Icons" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css">
    <link href="Site/css/material-dashboard.css?v=2.1.2" rel="stylesheet" />
     <link href="Site/demo/demo.css" rel="stylesheet" />
    
    <script defer src="Site/fontawesome/js/all.js"></script> 
    <link rel="stylesheet" href="Site/css/style.css" type="text/css">

 

 

</head>
<body>
 
 
 <jsp:include page="Header.jsp"/>
    


<section>

    <div class="row">


        <div class="col-md-12">

            <div class="row">

                <div class="col-md-4">
                    <div class="card card-chart">
                        <div class="card-header card-header-info">
                          <div class="ct-chart" id="dailySalesChart"></div>
                        </div>
                        <div class="card-body">
                          <h4 class="card-title">Ventas Diarias</h4>
                          <p class="card-category">
                            <span class="text-success"><i class="fa fa-long-arrow-up"></i> 55% </span> Incremento en ventas diarias.</p>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons">access_time</i> Actualizado hace 4 minutos
                          </div>
                        </div>
                      </div>
                </div>


                <div class="col-md-4">
                    <div class="card card-stats">
                        <div class="card-header card-header-info card-header-icon">
                          <div class="card-icon">
                            <i class="material-icons">store</i>
                          </div>
                          <p class="card-category">Total del dia</p>
                          <h3 class="card-title">$100.000</h3>
                        </div>
                        <div class="card-footer">
                          <div class="stats">
                            <i class="material-icons">date_range</i> Ultimas 24Hrs
                          </div>
                        </div>
                      </div>
            </div>


        </div>







        <div class="col-md-12 text-center">
             <div class="row">
                <div class="col-md-12">
                    <h1>
                        Ultimas Ventas
                    </h1>
                </div> 
             </div>   
        </div>

        <div class="col-md-12">
            <table class="table table-light table-striped">
                <thead>
                        <tr>
                          <th>id</th>
                            <th>nombre</th>
                              <th>monto</th>
                                <th>detalle</th>                       
                                 </tr>
                                  </thead>

                <tbody class="tbody" name="bodyMain">    
                    </tbody>
                        </table>
         </div>



    </div>


</section>

</body>

<script src ="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script src="Site/js/main.js"></script>
<script src="Site/js/Validaciones.js"></script>
<script src ="Site/js/notification/js/bootstrap-msg.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script>

</html>