







/*DESHABILITA BOTON FORMULARIO */

	function deshabilitarBoton(boolean,btn){
		
		var boton= "#"+btn+""
		$(boton).prop('disabled',!boolean);
	
 	}
		

  
/*VALIDA Apellido*/ 

	function validarApellido(apellido,btn){
		if(apellido.length<4){
		 
			alert("Apellido Invalido")
			$("#usuarioNuevoApellido").val("");
			deshabilitarBoton(false,btn)
  
		  }else if(apellido.includes(".")){
			
				alert("El nombre contiene puntos");
				$("#usuarioNuevoApellido").val("");
				deshabilitarBoton(false,btn)
			  
				}else{
				  deshabilitarBoton(true,btn)
				  	}
		}



/*VALIDA Nombre */

	function validarNombre(nombre,btn){
		console.log(btn)
		if(nombre.length<4){
  
			alert("nombre invalido");
			$("#usuarioNuevoNombre").val("");
			deshabilitarBoton(false,btn)
  
			}else if(nombre.includes(".")){
			  alert("El nombre contiene puntos");
			  $("#usuarioNuevoNombre").val("");
			  deshabilitarBoton(false,btn)
			  
				}else{
				  deshabilitarBoton(true,btn)
					}
	  }
  


/* SOLO PERMITE CARACTERES AZaz EN LOS NOMBRES Y APELLIDOS  */

$('#usuarioNuevoNombre,#usuarioNuevoApellido').keypress(function (tecla) {
	
    if (tecla.charCode < 64 || tecla.charCode > 123){
	
			if(tecla.charCode==32){
				return true;
			}
			
			return false;
	}
	
	
});










/* VERIFICA QUE EL ISBN EXISTA EN LA BD */

function validaISBN(isbn,btn){
	
	if(isbn.length<=7){
		
		deshabilitarBoton(false,btn)
		alert("isbn invalido")
		
	}else{
		
		dataISBN = {
			opc:2,
			isbn: isbn
			}
	
				$.ajax({
					
					url:"validaciones.do",
					type:"POST",
					data: dataISBN,
					success:function(response){
						if(response=="1"){
							
							console.log("Libro existente en BD")
							deshabilitarBoton(true,btn)
								
								copiasLibro(isbn,btn)
							
							}else{
								alert("libro no existe en BD")
								deshabilitarBoton(false,btn)
							}
						}
					})
					
					
				}
  }



/* BUSCA COPIAS DE LIBRO Y LAS AGREGA AL SELECTOR PARA LA VENTA*/

 function copiasLibro(isbn,btn){
	
	console.log(btn)
	dataCopias={
	opc:3,
	isbn:isbn
	}
		
		$.ajax({
		
			url:"Vender.do",
			type:"POST",
			data:dataCopias,
			success: function(response){
				
				if(response.length>10){
				var JSONcopiaLibro = JSON.parse(response)	
				console.log(JSONcopiaLibro.idCopia)
				$('#cantidad').append("<option value="+JSONcopiaLibro.idCopia+" id="+JSONcopiaLibro.Precio +"> Copia "+JSONcopiaLibro.idCopia+" Del  Libro "+JSONcopiaLibro.Titulo+" Precio: "+JSONcopiaLibro.Precio  +" </option>")
			
				}else{
					$('#cantidad').append("<option value=1>NO HAY COPIA PARA VENTA</option>")
					deshabilitarBoton(false,btn)
					}
				
			
			
			
			}		
		})
	
	
	}




/* VERIFICA QUE EL USUARIO ESTE CREADO EN LA VENTANA DE VENTA */

function rutVenta(rut,btn){
	
	dataRutVenta = {
		opc:1,
		rutPersona:rut}
	
		$.ajax({
			
			url:"validaciones.do",
			type:"POST",
			data: dataRutVenta,
			success:function(response){
				if(response=="-1"){
					alert("El cliente no existe, favor agregar")
					/*deshabilitarBoton(false,btn)*/
			
				}else{
					console.log("Cliente existente")
					/*deshabilitarBoton(true,btn)*/
					
					
				}
			 }
		})
 }



/*VALIDA QUE EL TRABAJADOR ESTE CREADO*/


function validaTrabajador(rut){
	
	data ={ rut: rut, opc:3 }
	
		$.ajax({
			
			url:"validaciones.do",
			type:"POST",
			data: data,
			success: function(response){
				
				if(response == "1"){
					
					console.log("trabajador ya existe")
					$("#btnAgregarTrabajador").prop('disabled',true)
					
						}else{
					
								console.log("trabajador no existe");
							}	
			 }
		})
	}







/*AGREGA AL USUARIO A LA BD*/
$('#btnAgregarCliente').click(function(event) {


      var nombre = $('#usuarioNuevoNombre').val();
      var apellido = $('#usuarioNuevoApellido').val();
      var fechaNacimiento = $('#fechaNacimiento').val();
      var rutPersona = $('#rutNuevoUsuario').val();
		
		console.log(fechaNacimiento);

		if(nombre=="" || apellido =="" || fechaNacimiento=="" || rutPersona =="" ){
			
			alert("Favor ingresar todos los campos")
			
			} else{
				
				var data = {
				opc: 1 ,
				nombre: nombre,
				apellido: apellido,
				fechaNacimiento: fechaNacimiento,
				rutPersona: rutPersona
			   }  		
		
				$.ajax({
					
					url:"Vender.do",
					type: "POST",
					data: data,
					success: function(response){
						
							console.log("persona agregada a la bd")	
							$(location).attr('href',"Vender.do");	
										
							}
						})
					}	
    });











/*VALUDA RUT*/
function validaRut(campo,btn){

	if ( campo.length == 0 ){ 
		
		alert("El rut no cumple con el formato");
			$("#rutNuevoUsuario").val("");
			deshabilitarBoton(false,btn)
		
		
		return false
	}
	
	if ( campo.length < 8 ){
		
		alert("El rut no cumple con el formato");
			$("#rutNuevoUsuario").val("");
				deshabilitarBoton(false,btn)
				return false
				
	}
	
	rutCompleto = campo
	campo = campo.replace('-','')
	campo = campo.replace(/\./g,'')

	var suma = 0;
	var caracteres = "1234567890kK";
	var contador = 0;   
	 
		for (var i=0; i < campo.length; i++){
			u = campo.substring(i, i + 1);
			if (caracteres.indexOf(u) != -1)
			contador ++;
		}
		
		if ( contador==0 ) { return false }
		
		var rut = campo.substring(0,campo.length-1)
		var drut = campo.substring( campo.length-1 )
		var dvr = '0';
		var mul = 2;
		
			for (i= rut.length -1 ; i >= 0; i--) {
				suma = suma + rut.charAt(i) * mul
		                if (mul == 7) 	mul = 2
				        else	mul++
			}
		
				res = suma % 11
				
				if (res==1)		dvr = 'k'
			    else if (res==0) dvr = '0'
				else {
					dvi = 11-res
					dvr = dvi + ""
				}
				if (dvr != drut.toLowerCase()) { 
					
						deshabilitarBoton(false,btn)
					    alert("rut invaludo");
						$("#rutNuevoUsuario").val("");
			
					} else { 
						
						deshabilitarBoton(true,btn),
							console.log("Rut correcto"),
									rutVenta(rutCompleto,btn) }
						}