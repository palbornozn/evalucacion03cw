package cl.inacap.FastDevelopment.Controllers;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.BoletaServiceLocal;
import Service.ClienteServiceLocal;
import Service.CopiaLibrosServiceLocal;
import Service.DetalleBoletaServiceLocal;
import Service.TrabajadorServiceLocal;
import cl.inacap.data.Boleta;
import cl.inacap.data.Cliente;
import cl.inacap.data.CopiaLibros;
import cl.inacap.data.DetalleBoleta;
import cl.inacap.data.Trabajador;



/**
 * Servlet implementation class Vender
 */
@WebServlet("/Vender.do")
public class Vender extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Vender() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Inject 
    private ClienteServiceLocal cs;
    @Inject
    private CopiaLibrosServiceLocal cl;
    @Inject
    private BoletaServiceLocal bs;
    @Inject 
    private TrabajadorServiceLocal ts;
    @Inject 
    private DetalleBoletaServiceLocal ds;
   
    
    
    public static Date formatoFecha(String fecha) {
    	try {
     return new	SimpleDateFormat("YYYY-MM-dd").parse(fecha);
    	}catch(Exception ex) {
    		return null;
    	}
    }
    	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	    List<CopiaLibros> copiaLibros = new ArrayList<>();
		copiaLibros = cl.listaCopiaLibros();
		
		
		
		
		
		
	
		request.setAttribute("copiaLibros",copiaLibros);
		request.getRequestDispatcher("Site/Vender.jsp").forward(request, response);
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	
		
		
		String opc = request.getParameter("opc");
		
		if(opc.equals("1")) {
		Date fechaNacimiento =  formatoFecha(request.getParameter("fechaNacimiento").toString());
		System.out.println(fechaNacimiento);
		Cliente c = new Cliente();
		

		c.setNombre(request.getParameter("nombre"));
		c.setApellidos(request.getParameter("apellido"));
		c.setFechaNacimiento(fechaNacimiento);
		c.setDNI(request.getParameter("rutPersona"));
		cs.insertCliente(c);
		
		
		response.sendRedirect("Vender.do");
		
		
		}
		
		
	
		
		if(opc.equals("2")) {
		
	
		int isbn = Integer.parseInt(request.getParameter("isbn"));
			
		List<CopiaLibros> copiaLibros = new ArrayList<>();
		copiaLibros = cl.listaCopiaLibros();
		
		
		
		String concatena = "{";
		
			concatena+= " \"isbn\": \""+ isbn +"\",";	
		for( CopiaLibros clb: copiaLibros) {
			
			if(isbn == clb.getLibro().getCod_isbn()){
				 
				 concatena += " \"titulo\": \"" +  clb.getLibro().getTitulo() + "\", \"precio\": \"" + clb.getPrecioLibro().getPrecio();
				
				break;
			}
		}
		
		concatena += "\" }";
		
		
		response.getWriter().write(concatena);
			
		}
		
		
		/*RETORNA INFORMACION DEL LIBRO ************/
		
		int idCopia=0;
		String titulo="No hay copias";
		int estado=0;
		int precioCopia =0;
		
		
		if(opc.equals("3")) {
			
			int isbn = Integer.parseInt(request.getParameter("isbn"));
			 List<CopiaLibros> copiaLibros = new ArrayList<>();
			 copiaLibros = cl.listaCopiaLibros();
			 boolean valida = false;
			 
			 for(CopiaLibros clb : copiaLibros) {
				 
				 if(clb.getLibro().getCod_isbn()==isbn && clb.getId_estadoLibro()==1 && clb.getPrecioLibro().getId_tipoPrecio()==1) {
					 idCopia = clb.getId_copiaLibros();
					 titulo = clb.getLibro().getTitulo();
					 estado = clb.getId_estadoLibro();
					 precioCopia = clb.getPrecioLibro().getPrecio();
					 valida =true;
					 break;
				 }
			 }
			 
			 if(valida==true) {
			 
			 
				 String concatena ="{ \"idCopia\": \""+idCopia+ "\",\"Titulo\":\"" +titulo+ "\",\"Estado\":\"" + estado + "\",\"Precio\":\""+ precioCopia  +"\"}" ;
			 response.getWriter().write(concatena);
			 
			 }else {
				 
				 response.getWriter().write("SinCopias");
			 }
		}
		
		
		
		
		
		
		
		/****METODO DE INGRESO DE BOLETA*********/
		
		if(opc.equals("4")) {
			
			String rutCliente =request.getParameter("rutCliente");
			int idCliente =0;
			
			List<Cliente> Clientes = cs.listarCliente();
			
			for(Cliente c : Clientes) {
				if(c.getDNI().equals(rutCliente)) {
					idCliente=c.getId_cliente();
					break;
				}
			}
			
			
			
			/*INGRESA NUEVA BOLETA*/
			
			int trabajador = 1;
			
			List<Trabajador> trabajadores = ts.listaTrabajador();
			
				for(Trabajador t : trabajadores) {
					if(t.getId_trabajador()==trabajador);
					
				}
				
				 
			/*Date fechaVenta = formatoFecha(request.getParameter("fechaDeVenta").toString());*/
			
		
	
			int precioNeto = Integer.parseInt(request.getParameter("precioNeto"));
			int precioIva = Integer.parseInt(request.getParameter("precioIva"));
			
			
			
			DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
			LocalDateTime now = LocalDateTime.now();
			
			
			Boleta b = new Boleta();
			
			b.setCliente(Clientes.get(idCliente));
			b.setFechaDeVenta(now);
			b.setHoraVenta(now);
			b.setPrecioConIva(precioIva);
			b.setPrecioNeto(precioNeto);
			b.setTotal(precioIva);
			b.setTrabajador(trabajadores.get(trabajador));
			b.setEstado_registro(false);
			
			
			bs.insertBoleta(b);
			
			List<CopiaLibros> copiaLibros = new ArrayList<>();
			copiaLibros = cl.listaCopiaLibros();
			
			
			int copiaLibro = Integer.parseInt(request.getParameter("copiaLibro"));
	
			int copia =1;
			for(CopiaLibros c : copiaLibros) {
				
				if(c.getId_copiaLibros()==copiaLibro) {
					copia = c.getId_copiaLibros();
					
					CopiaLibros LibroVendido = new CopiaLibros();
					LibroVendido.setDestino(c.getDestino());
					LibroVendido.setEstado_registro(c.isEstado_registro());
					LibroVendido.setId_copiaLibros(c.getId_copiaLibros());
					LibroVendido.setId_estadoLibro(3);
					LibroVendido.setNumeroSerie(c.getNumeroSerie());
					LibroVendido.setPrecioLibro(c.getPrecioLibro());
					
					cl.updateCopuaLibros(LibroVendido);
					
					break;
				}
			}
			
			
			DetalleBoleta detalle = new DetalleBoleta();
			
			detalle.setBoleta(b);
			detalle.setCopialibro(copiaLibros.get(copia-1));
			detalle.setValorLibro(precioIva);
			
			ds.insertDetalleBoleta(detalle);
			
			
		
			
			
			
		
			
		}	
		
		
		
		
	}
}
