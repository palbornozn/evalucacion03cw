package cl.inacap.FastDevelopment.Controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.UsuariosServiceLocal;
import cl.inacap.data.Usuarios;

/**
 * Servlet implementation class Login
 */
@WebServlet("/Login.do")
public class Login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Login() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Inject 
    private UsuariosServiceLocal us;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
		
		
		request.getRequestDispatcher("Site/Login.jsp").forward(request, response);
		
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
	 String nombreUsuario =	request.getParameter("nombreUsuario");
	 String contraseña =	request.getParameter("contrasena");
		
		List<Usuarios> listaUsuarios = new ArrayList<>();
		listaUsuarios = us.listarUsuarios();
		
		
		for(Usuarios u : listaUsuarios) {
			if(nombreUsuario.equals(u.getNombreUsuario()) && contraseña.equals(u.getContraseña())) {
				/*response.sendRedirect("Home.do");*/
				response.getWriter().write("1");
				break;
			}	
		}
		
		
		response.getWriter().write("-1");
		
		
		
		
	}

}
