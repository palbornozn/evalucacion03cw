package cl.inacap.FastDevelopment.Controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import Service.TrabajadorServiceLocal;
import cl.inacap.data.Trabajador;

/**
 * Servlet implementation class Trabajadores
 */
@WebServlet("/Trabajadores.do")
public class Trabajadores extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public Trabajadores() {
        super();
        // TODO Auto-generated constructor stub
    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
    @Inject
    private TrabajadorServiceLocal ts;
    
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		List<Trabajador> listaTrabajador = new ArrayList<>();
		listaTrabajador = ts.listaTrabajador();
		
		request.setAttribute("listaTrabajador", listaTrabajador);
		request.getRequestDispatcher("Site/ManTrabajadores.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String opc = request.getParameter("opc");
		if(opc.equals("1")) {
		String rut = request.getParameter("rut");
		String Nombre = request.getParameter("nombre");
		String Apellido = request.getParameter("apellido");
		
		Trabajador t = new Trabajador();
		
			t.setDNI(rut);
			t.setNombre(Nombre);
			t.setApellidos(Apellido);
			
			ts.insertTrabajador(t);
			
			response.sendRedirect("Trabajadores.do");
		} else if(opc.contentEquals("2")) {
			List<Trabajador> listaTrabajador = new ArrayList<>();
			listaTrabajador = ts.listaTrabajador();
			String nuevoRut = request.getParameter("nuevoRut");
			boolean valida = false;
			
			for(Trabajador t : listaTrabajador) {
				if(nuevoRut.equals(t.getDNI())) {
					valida = true;
					break;
				}
			}
			
			response.getWriter().print((valida)?"1":"-1");
		}
		
	}

}
